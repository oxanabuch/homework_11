package com.company.wrappers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StringTest {

    public static void main(String[] args) {

        // ----------------- 3 -------------------------

        // 1 - 4
        String s1 = "Hello";
        String s2 = new String("The word is big");
        String[] s3 = new String[]{"The", "word", "is", "big"};
        byte[] s4 = s2.getBytes();

        String s5 = "Апельсин,Яблоко,Гранат,Груша";

        String[] splitS5 = s5.split(",");

        int maxLength = 0;
        int index = 0;
        int currentIndex = 0;

        List<String> arrayList = Arrays.stream(splitS5).toList();

        for (String str : arrayList) {

            if (str.length() > maxLength) {
                maxLength = str.length();
                index = currentIndex;
            }

            currentIndex++;

        }

        String longestWord = arrayList.get(index);

        System.out.println(longestWord.toLowerCase());

        // 5
        String newLine = "   Я_новая_строка      ";
        // 6
        newLine = newLine.trim();
        // 7
        newLine = newLine.replaceAll("_", " ");
        // 8
        System.out.println(newLine);

        // 9
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Введите текст тут внизу и потом нажмите enter");

        String input = myObj.nextLine();
        System.out.println("Вы ввели: " + input);

        boolean turnOff = false;

        while (!turnOff){

            // 10
            if(input.equals("Запуск"))  {
                System.out.println("Запускаем процесс");
                break;
            }

            // 11
            if (input.contains("завершен")) {
                System.out.println("Процесс завершен");
                turnOff = true;
            }

            // 12
            if (input.toLowerCase().contains("ошибка")) {
                System.out.println("Произошла ошибка");
                break;
            }

        }


        // ----------------- 4 -------------------------

        // 1, 2
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(s1);
        stringBuilder.append(s2);
        stringBuilder.append("\n");
        stringBuilder.append(Arrays.toString(s3));
        stringBuilder.append(Arrays.toString(s4));
        stringBuilder.append(s5);
        stringBuilder.append("\n");
        stringBuilder.append(longestWord);
        stringBuilder.append(newLine);

        // 3
        stringBuilder.reverse();
        // 4
        System.out.print(stringBuilder.toString());

    }
}
