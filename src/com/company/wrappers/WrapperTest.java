package com.company.wrappers;

public class WrapperTest {

    
    public static void main(String[] args) {

        Integer i1 = 77_777;
        Integer i2 = Integer.valueOf(77_777);
        Integer i3 = new Integer(77_777);
        Integer i4 = Integer.valueOf("77777");


        Character ch1 = 'A';
        Character ch2 =  Character.valueOf('A');
        Character ch3 = new Character('A');
        // Character ch4 = Character.parse;

        Byte b1 = 125;
        Byte b2 = Byte.valueOf((byte) 125);
        Byte b3 = new Byte((byte) 125);
        Byte b4 = Byte.parseByte("125");

        Short s1 = 31_555;
        Short s2 = Short.valueOf((short) 31_555);
        Short s3 = (short) 31555;
        Short s4 = new Short((short) 31555);


        Long l1 = 100_000_000_000_000L;
        Long l2 = Long.valueOf(100_000);
        Long l4 = new Long(100_000_000_000L);
        Long l3 = Long.parseLong("100000");

        Float f1 = 53_453_000.55f;
        Float f2 = Float.valueOf("53453000.55");
        Float f3 = new Float(53_453_000.55f);
        Float f4 = Float.parseFloat("53453000.55");

        Double d1 = 100_000_000_000_000.07;
        Double d2 = Double.valueOf(100_000_000_000_000.07);
        Double d3 = new Double(100_000_000_000_000.07);
        Double d4 = Double.parseDouble("10000.07");

        Boolean bl1 = true;
        Boolean bl2 = Boolean.valueOf("true");
        Boolean bl3 = new Boolean(true);
        Boolean bl4 = Boolean.parseBoolean("true");


        //------

        double dbl = 3.2;


        double tempForInt = Double.valueOf(i1) + dbl;
        Integer intTemp = (int) tempForInt;             // short solution: ch1 = (Character) (char) temp;
        i1 = intTemp;

        tempForInt = Double.valueOf(i2) + dbl;
        intTemp = (int) tempForInt;
        i2 = intTemp;

        tempForInt = Double.valueOf(s3) + dbl;
        intTemp = (int) tempForInt;
        i3 = intTemp;

        tempForInt = Double.valueOf(i4) + dbl;
        intTemp = (int) tempForInt;
        i4 = intTemp;

        //------------

        double tempForChar = Double.valueOf(ch1) + dbl;
        Character chTemp = (char) tempForChar;             // short solution: ch1 = (Character) (char) temp;
        ch1 = chTemp;

        tempForChar = Double.valueOf(ch2) + dbl;
        chTemp = (char) tempForChar;
        ch2 = chTemp;

        tempForChar = Double.valueOf(ch3) + dbl;
        chTemp = (char) tempForChar;
        ch3 = chTemp;

        //------------

        double tempForByte = Double.valueOf(b1) + dbl;
        Byte bTemp = (byte) tempForByte;             // short solution: ch1 = (Character) (char) temp;
        b1 = bTemp;

        tempForByte = Double.valueOf(b2) + dbl;
        bTemp = (byte) tempForByte;
        b2 = bTemp;

        tempForByte = Double.valueOf(b3) + dbl;
        bTemp = (byte) tempForByte;
        b3 = bTemp;

        tempForByte = Double.valueOf(b4) + dbl;
        bTemp = (byte) tempForByte;
        b4 = bTemp;

        //------------

        double tempForShort = Double.valueOf(s1) + dbl;
        Short shTemp = (short) tempForShort;             // short solution: ch1 = (Character) (char) temp;
        s1 = shTemp;

        tempForShort = Double.valueOf(s2) + dbl;
        shTemp = (short) tempForShort;
        s2 = shTemp;

        tempForShort = Double.valueOf(s3) + dbl;
        shTemp = (short) tempForShort;
        s3 = shTemp;

        tempForShort = Double.valueOf(s4) + dbl;
        shTemp = (short) tempForShort;
        s4 = shTemp;

        // --------

        double tempForLong = Double.valueOf(l1) + dbl;
        Long lngTemp = (long) tempForLong;
        l1 = lngTemp;

        tempForLong = Double.valueOf(l2) + dbl;
        lngTemp = (long) tempForLong;
        l2 = lngTemp;

        tempForLong = Double.valueOf(l3) + dbl;
        lngTemp = (long) tempForLong;
        l3 = lngTemp;

        tempForLong = Double.valueOf(l4) + dbl;
        lngTemp = (long) tempForLong;
        l4 = lngTemp;



        // --------

        double tempForFloar = Double.valueOf(f1) + dbl;
        Float floatTemp = (float) tempForFloar;
        f1 = floatTemp;

        tempForFloar = Double.valueOf(f2) + dbl;
        floatTemp = (float) tempForFloar;
        f2 = floatTemp;

        tempForFloar = Double.valueOf(f3) + dbl;
        floatTemp = (float) tempForFloar;
        f3 = floatTemp;

        tempForFloar = Double.valueOf(f4) + dbl;
        floatTemp = (float) tempForFloar;
        f4 = floatTemp;

        //-----------Wrapper классы (Практика)----------------

        // 4	Проверить значения NaN и Infinity.

        double nullDouble = 0.0;
        double noNullDouble = 3.0;

        double nanValue = Double.NaN;
        nanValue = nullDouble / noNullDouble;

        double infinityValue = Double.POSITIVE_INFINITY;
        infinityValue = infinityValue / nullDouble;

        System.out.println("nanValue: " + nanValue);
        System.out.println("infinityValue: " + infinityValue);

        if (Double.isFinite(nanValue)) {
            System.out.println("Переменная {{" + nanValue + "}} = Infinity");
        } else if (Double.isNaN(nanValue)) {
            System.out.println("Переменная {{" + nanValue + "}} = NaN");
        }

        if (Double.isFinite(infinityValue)) {
            System.out.println("Переменная {{" + infinityValue + "}} = Infinity");
        } else if (Double.isNaN(infinityValue)) {
            System.out.println("Переменная {{" + infinityValue + "}} = NaN");
        }

        // 5

        // 5.1
        long long1 = 120;
        long long2 = 120;

        boolean longComparison = long1 == long1;

        System.out.println(longComparison);

        // 5.2
        long1 = 1200;
        long2 = 1200;

        longComparison = long1 == long1;
        System.out.println(longComparison);

    }
}
